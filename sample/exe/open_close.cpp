#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <Windows.h>

int __cdecl main(int argc,
                 char* argv[])
{
    CHAR myPidFile[100];
    DWORD myPid = 0;

    printf("Entre com um PID (ou zero para atual):");
    scanf("%d", &myPid);

    if( myPid == 0 )
    {
        myPid = GetCurrentProcessId();
        printf("Usando PID atual %d\n", myPid);
    }        
    else printf("Usando PID fornecido %d\n", myPid);

    sprintf(myPidFile, "\\\\.\\OpenClose\\%d", myPid);

    HANDLE hDev = CreateFile(myPidFile, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

    if( hDev != INVALID_HANDLE_VALUE )
    {
        CloseHandle(hDev);
    }
    else printf("Erro %d abrindo dispositivo em %s\n", GetLastError(), myPidFile);

    return 0;
}

