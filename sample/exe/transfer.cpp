#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <windows.h>

int main(int argc, char* argv[])
{
	HANDLE device_handle = NULL;

	__try
	{
		device_handle = CreateFile("\\\\.\\Transfer",
							 GENERIC_READ | GENERIC_WRITE,
							 0,
							 NULL,
							 OPEN_EXISTING,
							 0,
							 NULL);

		if (device_handle == INVALID_HANDLE_VALUE)
		{
			printf("Erro #%d ao abrir device.\n", GetLastError());
			__leave;
		}

		char data[4] = {'O','L', 'A', '\0'};
		DWORD written;

		WriteFile(device_handle, data, 4, &written, NULL);
		printf("OLA Written\n");
		_getch();

		data[0] = '\0';
		DWORD read;
		ReadFile(device_handle, data, 4, &read, NULL);
		printf("%s Read", data);
		_getch();
	}
	__finally
	{
		if(device_handle)
			CloseHandle(device_handle);
	}

	return 0;
}