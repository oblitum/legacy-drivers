#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <windows.h>

int __cdecl main(int argc, char* argv[])
{
	HANDLE hDevice = NULL;
	STARTUPINFO StartupInfo = {};
	PROCESS_INFORMATION ProcessInfo = {};

	char szNotepadPath[MAX_PATH];

	GetWindowsDirectory(szNotepadPath, sizeof(szNotepadPath));

	strcat_s(szNotepadPath, sizeof(szNotepadPath), "\\notepad.exe");

	__try
	{
		hDevice = CreateFile("\\\\.\\Cleanup",
							 GENERIC_READ,
							 0,
							 NULL,
							 OPEN_EXISTING,
							 0,
							 NULL);

		if (hDevice == INVALID_HANDLE_VALUE)
		{
			printf("Erro #%d ao abrir device.\n",
				   GetLastError());
			__leave;
		}

		printf("Device aberto com sucesso!\n");
		
		_getch();

		if(!CreateProcess(NULL,
			szNotepadPath,
			NULL,
			NULL,
			FALSE,
			0,
			NULL,
			NULL,
			&StartupInfo,
			&ProcessInfo))
		{
			printf("Erro #%d ao criar processo. \n",
				GetLastError());

			__leave;
		}

		printf("Success on create process \n");

		if(!DuplicateHandle(GetCurrentProcess(), hDevice, ProcessInfo.hProcess, NULL, 0, FALSE, DUPLICATE_SAME_ACCESS))
		{
			printf("Erro #%d ao duplicar handle.\n", GetLastError());
			_getch();
		}

		printf("Success on DuplicateHandle \n");

		_getch();
	}
	__finally
	{
		if(ProcessInfo.hProcess)
			CloseHandle(ProcessInfo.hProcess);

		if(ProcessInfo.hThread)
			CloseHandle(ProcessInfo.hThread);

		if(hDevice)
			CloseHandle(hDevice);
	}
}