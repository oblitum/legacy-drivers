#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <Windows.h>

int __cdecl main(int argc,
                 char* argv[])
{
    char*   szBuffer = NULL;

    szBuffer = (char*)VirtualAlloc(NULL,
                                   100,
                                   MEM_COMMIT | MEM_TOP_DOWN,
                                   PAGE_READWRITE);

    printf("Process ID = %d\n", GetCurrentProcessId());
    printf("Buffer em 0x%p\n",
           szBuffer);

    printf("Entre com uma string qualquer: ");

    scanf_s("%s", szBuffer, 99);
    printf("String = %s\n",
           szBuffer);

    puts("\nTecle algo para terminar.");
    _getch();

    VirtualFree(szBuffer,
                0,
                MEM_RELEASE);

    return 0;
}

