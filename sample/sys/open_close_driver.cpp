#include <ntddk.h>
#include <Ntstrsafe.h>

#define DEVICE_PATH L"\\Device\\OpenClose"
#define DEVICE_SYMBOLIC_LINK_PATH L"\\DosDevices\\OpenClose"


extern "C" void Unload(PDRIVER_OBJECT drvObj)
{
    UNICODE_STRING devSymLink = RTL_CONSTANT_STRING(DEVICE_SYMBOLIC_LINK_PATH);
    IoDeleteSymbolicLink(&devSymLink); // n�o importa se existe ou n�o

    if( drvObj->DeviceObject )
        IoDeleteDevice(drvObj->DeviceObject);

    DbgPrint("Hello, Kernel v. " __DATE__ " " __TIME__ ": UNLoad\n");
}


extern "C" NTSTATUS OnCreateClose(PDEVICE_OBJECT devObj, PIRP irp)
{
    PIO_STACK_LOCATION stack = IoGetCurrentIrpStackLocation(irp);
    PFILE_OBJECT fileObj = stack->FileObject;
    ULONG currPid = (ULONG) PsGetCurrentProcessId();
    ULONG filePid = 0;

    // vamos escapar a barra inicial '\' para pegar o n�mero puro e converter
    if( fileObj->FileName.Length >= 1 )
    {
        UNICODE_STRING currPidStr;
        RtlUnicodeStringInit(&currPidStr, &fileObj->FileName.Buffer[1]);
        RtlUnicodeStringToInteger(&currPidStr, 10, &filePid);
    }

    if( currPid == filePid )
    {
        irp->IoStatus.Status = STATUS_SUCCESS;
        irp->IoStatus.Information = 0;
        DbgPrint("HelloKernel: OnCreateClose\n");
    }
    else
    {
        irp->IoStatus.Status = STATUS_ACCESS_DENIED;
        irp->IoStatus.Information = 0;
        DbgPrint("HelloKernel: Error comparing current pid\n");
    }

    return STATUS_SUCCESS;
}


extern "C" NTSTATUS DriverEntry(PDRIVER_OBJECT drvObj, PUNICODE_STRING regPath)
{
    UNICODE_STRING devPath = RTL_CONSTANT_STRING(DEVICE_PATH);
    UNICODE_STRING devSymLink = RTL_CONSTANT_STRING(DEVICE_SYMBOLIC_LINK_PATH);
    PDEVICE_OBJECT devObj = NULL;

    drvObj->DriverUnload = Unload;

    drvObj->MajorFunction[IRP_MJ_CREATE] = OnCreateClose;
    drvObj->MajorFunction[IRP_MJ_CLOSE] = OnCreateClose;

    NTSTATUS nts = IoCreateDevice(drvObj, 0, &devPath, 
        FILE_DEVICE_UNKNOWN, 0, FALSE, &devObj);

    if( NT_SUCCESS(nts) )
    {
        nts = IoCreateSymbolicLink(&devSymLink, &devPath);
        if( NT_ERROR(nts) )
            IoDeleteDevice(devObj);
    }

    DbgPrint("Hello, Kernel v. " __DATE__ " " __TIME__ ": Load\n");
    return nts;
}

