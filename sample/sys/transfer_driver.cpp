#include <ntddk.h>

#define TAG_TRANSFER 'SNRT'

PVOID saved_data = NULL;
ULONG saved_data_size = 0;

VOID
OnDriverUnload(PDRIVER_OBJECT   pDriverObj)
{
    UNICODE_STRING  usSymbolicLink = RTL_CONSTANT_STRING(L"\\DosDevices\\Transfer");

    IoDeleteSymbolicLink(&usSymbolicLink);
    IoDeleteDevice(pDriverObj->DeviceObject);
}

NTSTATUS
OnCreate(PDEVICE_OBJECT pDeviceObj,
         PIRP           pIrp)
{
	DbgPrint("%s", "Create Called\n");

    pIrp->IoStatus.Status = STATUS_SUCCESS;
    pIrp->IoStatus.Information = 0;

    IoCompleteRequest(pIrp, IO_NO_INCREMENT);
    return STATUS_SUCCESS;
}

NTSTATUS
OnCleanup(PDEVICE_OBJECT pDeviceObj,
         PIRP           pIrp)
{
	DbgPrint("%s", "Cleanup Called\n");

    pIrp->IoStatus.Status = STATUS_SUCCESS;
    pIrp->IoStatus.Information = 0;

    IoCompleteRequest(pIrp, IO_NO_INCREMENT);
    return STATUS_SUCCESS;
}

NTSTATUS
OnClose(PDEVICE_OBJECT  pDeviceObj,
        PIRP            pIrp)
{
	DbgPrint("%s", "Close Called\n");

    pIrp->IoStatus.Status = STATUS_SUCCESS;
    pIrp->IoStatus.Information = 0;

    IoCompleteRequest(pIrp, IO_NO_INCREMENT);
    return STATUS_SUCCESS;
}

NTSTATUS
OnWrite(PDEVICE_OBJECT  pDeviceObj,
        PIRP            pIrp)
{
	DbgPrint("%s", "Write Called\n");

	PIO_STACK_LOCATION stack_location = IoGetCurrentIrpStackLocation(pIrp);
	ULONG write_size = stack_location->Parameters.Write.Length;

	if(saved_data)
	{
		ExFreePoolWithTag(saved_data, TAG_TRANSFER);
		saved_data = NULL;
		saved_data_size = 0;
	}
	saved_data = ExAllocatePoolWithTag(PagedPool, write_size, TAG_TRANSFER); 
	RtlCopyMemory(saved_data, pIrp->AssociatedIrp.SystemBuffer, write_size);
	saved_data_size = write_size;

	pIrp->IoStatus.Information = write_size;
	pIrp->IoStatus.Status = STATUS_SUCCESS;
    IoCompleteRequest(pIrp, IO_NO_INCREMENT);
    return STATUS_SUCCESS;
}

NTSTATUS
OnRead(PDEVICE_OBJECT   pDeviceObj,
        PIRP            pIrp)
{
	DbgPrint("%s", "Read Called\n");

	PIO_STACK_LOCATION stack_location = IoGetCurrentIrpStackLocation(pIrp);
	ULONG read_size = stack_location->Parameters.Read.Length;

	if(read_size > saved_data_size)
	{
		pIrp->IoStatus.Information = 0;
		pIrp->IoStatus.Status = STATUS_INSUFFICIENT_RESOURCES;
		IoCompleteRequest(pIrp, IO_NO_INCREMENT);
		return STATUS_INSUFFICIENT_RESOURCES;
	}

	RtlCopyMemory(pIrp->AssociatedIrp.SystemBuffer, saved_data, read_size);
	ExFreePoolWithTag(saved_data, TAG_TRANSFER);
	saved_data = NULL;
	saved_data_size = 0;

	pIrp->IoStatus.Information = read_size;
    pIrp->IoStatus.Status = STATUS_SUCCESS;
	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
    return STATUS_SUCCESS;
}

extern "C" NTSTATUS
DriverEntry(PDRIVER_OBJECT  pDriverObj,
            PUNICODE_STRING pusRegistryPath)
{
    NTSTATUS        nts;
    UNICODE_STRING  usDeviceName = RTL_CONSTANT_STRING(L"\\Device\\Transfer");
    UNICODE_STRING  usSymbolicLink = RTL_CONSTANT_STRING(L"\\DosDevices\\Transfer");
    PDEVICE_OBJECT  pDeviceObj = NULL;

	DbgPrint("%s", "Let's Transfer\n");

    pDriverObj->DriverUnload = OnDriverUnload;

    pDriverObj->MajorFunction[IRP_MJ_CREATE] = OnCreate;
    pDriverObj->MajorFunction[IRP_MJ_CLOSE] = OnClose;
	pDriverObj->MajorFunction[IRP_MJ_CLEANUP] = OnCleanup;
	pDriverObj->MajorFunction[IRP_MJ_WRITE] = OnWrite;
	pDriverObj->MajorFunction[IRP_MJ_READ] = OnRead;

    nts = IoCreateDevice(pDriverObj,
                         0,
                         &usDeviceName,
                         FILE_DEVICE_UNKNOWN,
                         0,
                         FALSE,
                         &pDeviceObj);
    if (!NT_SUCCESS(nts))
        return nts;

	pDeviceObj->Flags |= DO_BUFFERED_IO;

    nts = IoCreateSymbolicLink(&usSymbolicLink,
                               &usDeviceName);
    if (!NT_SUCCESS(nts))
        IoDeleteDevice(pDeviceObj);

    return nts;
}
