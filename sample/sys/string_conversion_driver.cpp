#include <ntddk.h>

#define TAG_CONVERSION 'RVNC'

PVOID saved_data = NULL;
ULONG saved_data_size = 0;

VOID
OnDriverUnload(PDRIVER_OBJECT   pDriverObj)
{
    UNICODE_STRING  usSymbolicLink = RTL_CONSTANT_STRING(L"\\DosDevices\\Conversion");

    IoDeleteSymbolicLink(&usSymbolicLink);
    IoDeleteDevice(pDriverObj->DeviceObject);
}

NTSTATUS
OnCreate(PDEVICE_OBJECT pDeviceObj,
         PIRP           pIrp)
{
	DbgPrint("%s", "Create Called\n");

	PIO_STACK_LOCATION stack_location = IoGetCurrentIrpStackLocation(pIrp);

	//stack_location->FileObject->FsContext = 0;

    pIrp->IoStatus.Status = STATUS_SUCCESS;
    pIrp->IoStatus.Information = 0;
    IoCompleteRequest(pIrp, IO_NO_INCREMENT);
    return STATUS_SUCCESS;
}

NTSTATUS
OnCleanup(PDEVICE_OBJECT pDeviceObj,
         PIRP           pIrp)
{
	DbgPrint("%s", "Cleanup Called\n");

    pIrp->IoStatus.Status = STATUS_SUCCESS;
    pIrp->IoStatus.Information = 0;

    IoCompleteRequest(pIrp, IO_NO_INCREMENT);
    return STATUS_SUCCESS;
}

NTSTATUS
OnClose(PDEVICE_OBJECT  pDeviceObj,
        PIRP            pIrp)
{
	DbgPrint("%s", "Close Called\n");

    pIrp->IoStatus.Status = STATUS_SUCCESS;
    pIrp->IoStatus.Information = 0;

    IoCompleteRequest(pIrp, IO_NO_INCREMENT);
    return STATUS_SUCCESS;
}

NTSTATUS
OnWrite(PDEVICE_OBJECT  pDeviceObj,
        PIRP            pIrp)
{
	DbgPrint("%s", "Write Called\n");

	PIO_STACK_LOCATION stack_location = IoGetCurrentIrpStackLocation(pIrp);
	PVOID system_buffer = pIrp->AssociatedIrp.SystemBuffer;
	ULONG write_size = stack_location->Parameters.Write.Length;
	PVOID &context = stack_location->FileObject->FsContext;
	ANSI_STRING ansi_string = {write_size, write_size, (PCHAR)system_buffer};

	context = (PUNICODE_STRING)ExAllocatePoolWithTag(PagedPool, sizeof(UNICODE_STRING), TAG_CONVERSION);
	RtlAnsiStringToUnicodeString((PUNICODE_STRING)context, &ansi_string, TRUE);

	pIrp->IoStatus.Information = write_size;
	pIrp->IoStatus.Status = STATUS_SUCCESS;
    IoCompleteRequest(pIrp, IO_NO_INCREMENT);
    return STATUS_SUCCESS;
}

NTSTATUS
OnRead(PDEVICE_OBJECT   pDeviceObj,
        PIRP            pIrp)
{
	DbgPrint("%s", "Read Called\n");

	PIO_STACK_LOCATION stack_location = IoGetCurrentIrpStackLocation(pIrp);
	ULONG read_size = stack_location->Parameters.Read.Length;
	PVOID& system_buffer = pIrp->AssociatedIrp.SystemBuffer;
	PVOID &context = stack_location->FileObject->FsContext;
	ANSI_STRING ansi_string = {};

	if(context == NULL)
	{
		pIrp->IoStatus.Information = 0;
		pIrp->IoStatus.Status = STATUS_INVALID_PARAMETER;
		IoCompleteRequest(pIrp, IO_NO_INCREMENT);
		return STATUS_INVALID_PARAMETER;
	}

	if(read_size > ((PUNICODE_STRING)context)->Length)
	{
		pIrp->IoStatus.Information = 0;
		pIrp->IoStatus.Status = STATUS_INSUFFICIENT_RESOURCES;
		IoCompleteRequest(pIrp, IO_NO_INCREMENT);
		return STATUS_INSUFFICIENT_RESOURCES;
	}

	RtlUnicodeStringToAnsiString(&ansi_string, (PUNICODE_STRING)context, TRUE);
	RtlCopyMemory(pIrp->AssociatedIrp.SystemBuffer, ansi_string.Buffer, read_size);
	RtlFreeAnsiString(&ansi_string);
	RtlFreeUnicodeString((PUNICODE_STRING)context);
	ExFreePoolWithTag(context, TAG_CONVERSION);
	context = NULL;

	pIrp->IoStatus.Information = read_size;
    pIrp->IoStatus.Status = STATUS_SUCCESS;
	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
    return STATUS_SUCCESS;
}

extern "C" NTSTATUS
DriverEntry(PDRIVER_OBJECT  pDriverObj,
            PUNICODE_STRING pusRegistryPath)
{
    NTSTATUS        nts;
    UNICODE_STRING  usDeviceName = RTL_CONSTANT_STRING(L"\\Device\\Conversion");
    UNICODE_STRING  usSymbolicLink = RTL_CONSTANT_STRING(L"\\DosDevices\\Conversion");
    PDEVICE_OBJECT  pDeviceObj = NULL;

	DbgPrint("%s", "Let's Convert\n");

    pDriverObj->DriverUnload = OnDriverUnload;

    pDriverObj->MajorFunction[IRP_MJ_CREATE] = OnCreate;
    pDriverObj->MajorFunction[IRP_MJ_CLOSE] = OnClose;
	pDriverObj->MajorFunction[IRP_MJ_CLEANUP] = OnCleanup;
	pDriverObj->MajorFunction[IRP_MJ_WRITE] = OnWrite;
	pDriverObj->MajorFunction[IRP_MJ_READ] = OnRead;

    nts = IoCreateDevice(pDriverObj,
                         0,
                         &usDeviceName,
                         FILE_DEVICE_UNKNOWN,
                         0,
                         FALSE,
                         &pDeviceObj);
    if (!NT_SUCCESS(nts))
        return nts;

	pDeviceObj->Flags |= DO_BUFFERED_IO;

    nts = IoCreateSymbolicLink(&usSymbolicLink,
                               &usDeviceName);
    if (!NT_SUCCESS(nts))
        IoDeleteDevice(pDeviceObj);

    return nts;
}
